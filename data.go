package main

type MetaBound struct {
	Uid  string `json:"uid"`
	Pos  string `json:"s"`
	Next string `json:"s"`
}
