package main

import (
	"context"
	"encoding/json"
	"github.com/beevik/ntp"
	"github.com/go-kit/kit/endpoint"
	"net/http"
	"time"
)

func makeUppercaseEndpoint(svc StringService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(uppercaseRequest)
		v, err := svc.Uppercase(req.S)
		if err != nil {
			return uppercaseResponse{v, err.Error()}, nil
		}
		return uppercaseResponse{v, ""}, nil
	}
}

func makeCountEndpoint(svc StringService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(countRequest)
		v := svc.Count(req.S)
		return countResponse{v}, nil
	}
}

func makeBoundEndpoint(svc ManageService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(boundRequest)
		var metaBound MetaBound
		metaBound.Next = req.Next
		metaBound.Pos = req.Pos
		metaBound.Uid = req.Uid
		v := svc.Bound(metaBound)
		return boundResponse{v}, nil
	}
}

func makeEchoEndpoint(svc ManageService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		//req := request.(echoRequest)

		v := svc.Echo("Hello!")
		return echoResponse{v}, nil
	}
}

func makeRttEndpoint(svc ManageService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		//req := request.(echoRequest)
		response, _ := ntp.Query("0.beevik-ntp.pool.ntp.org")
		t := time.Now().Add(response.ClockOffset)
		clock := t.UnixNano()
		v := svc.Rtt(clock)
		return rttResponse{v}, nil
	}
}

func decodeUppercaseRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request uppercaseRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeCountRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request countRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeBoundRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request boundRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeEchoRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request echoRequest
	/*if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}*/
	request.GetEcho = "Echo"
	return request, nil
}

func decodeRttRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request rttRequest
	/*if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}*/
	request.GetClock = "Clock"
	return request, nil
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}

type uppercaseRequest struct {
	S string `json:"s"`
}

type uppercaseResponse struct {
	V   string `json:"v"`
	Err string `json:"err,omitempty"`
}

type countRequest struct {
	S string `json:"s"`
}

type countResponse struct {
	V int `json:"v"`
}

type boundRequest struct {
	Uid  string `json:"uid"`
	Pos  string `json:"pos"`
	Next string `json:"next"`
}
type echoRequest struct {
	GetEcho string
}
type rttRequest struct {
	GetClock string
}

type boundResponse struct {
	V bool `json:"v"`
}

type echoResponse struct {
	S string `json:"response"`
}
type rttResponse struct {
	Clockhost int64 `json:"clkhost"`
}
