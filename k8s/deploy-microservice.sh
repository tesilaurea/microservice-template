#!/bin/bash
export REPLICA=$5

for i in `seq 1 $REPLICA`
do
	export NAME=$1$i 
        export LABEL=$2$i 
        export DEPLOYMENT=$3$i
	export MICROSERVICE=$4 
	envsubst '${NAME} ${LABEL} ${DEPLOYMENT} ${MICROSERVICE}' <microservice.yaml >deploy-microservice.yaml
	kubectl create -f deploy-microservice.yaml
done



