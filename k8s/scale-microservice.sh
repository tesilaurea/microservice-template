#!/bin/bash
export REPLICA=$5

for i in `seq 1 $REPLICA`
do
	export NAME=$1$i 
        export LABEL=$2$i 
        export DEPLOYMENT=$3$i
	export MICROSERVICE=$4 
	envsubst '${NAME} ${LABEL} ${DEPLOYMENT} ${MICROSERVICE}' <microservice.yaml >deploy-microservice.yaml
	OUTPUT="$(kubectl get deployments $NAME 2>&1)"
	echo $OUTPUT | grep -q "NAME"
	if [ $? -eq 0 ];then
	  echo "found"
	fi
	echo $OUTPUT | grep -q "Error"
	if [ $? -eq 0 ];then
	  echo "not found"
	  kubectl create -f deploy-microservice.yaml

	fi
	#echo "$OUTPUT"
done
j=$(($REPLICA+1)) 
cond=0
while [ $cond==0 ]
do
	echo "Ciclo"$j
	export NAME=$1$j 
	export LABEL=$2$j 
	export DEPLOYMENT=$3$j
	export MICROSERVICE=$4
	envsubst '${NAME} ${LABEL} ${DEPLOYMENT} ${MICROSERVICE}' <microservice.yaml >deploy-microservice.yaml
	OUTPUT="$(kubectl get deployments $NAME 2>&1)"
	echo $OUTPUT | grep -q "NAME"
	if [ $? -eq 0 ];then
	  echo "found"
	  kubectl delete -f deploy-microservice.yaml
	  j=$(($j+1)) 
	  echo $j
	fi
	echo $OUTPUT | grep -q "Error"
	if [ $? -eq 0 ];then
	  echo "not found"
	  cond=1
	  break
	fi
done

