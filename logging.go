package main

import (
	"time"

	"github.com/go-kit/kit/log"
)

type loggingMiddleware struct {
	logger log.Logger
	next   StringService
}

type loggingMetaMiddleware struct {
	logger log.Logger
	next   ManageService
}

func (mw loggingMiddleware) Uppercase(s string) (output string, err error) {
	defer func(begin time.Time) {
		_ = mw.logger.Log(
			"method", "uppercase",
			"input", s,
			"output", output,
			"err", err,
			"took", time.Since(begin),
		)
	}(time.Now())

	output, err = mw.next.Uppercase(s)
	return
}

func (mw loggingMiddleware) Count(s string) (n int) {
	defer func(begin time.Time) {
		_ = mw.logger.Log(
			"method", "count",
			"input", s,
			"n", n,
			"took", time.Since(begin),
		)
	}(time.Now())

	n = mw.next.Count(s)
	return
}

func (mw loggingMetaMiddleware) Bound(metaBound MetaBound) (b bool) {
	defer func(begin time.Time) {
		_ = mw.logger.Log(
			"method", "bound",
			"input", metaBound,
			"b", b,
			"took", time.Since(begin),
		)
	}(time.Now())

	b = mw.next.Bound(metaBound)
	return
}

func (mw loggingMetaMiddleware) Echo(s string) (echo string) {
	defer func(begin time.Time) {
		_ = mw.logger.Log(
			"method", "echo",
			"input", s,
			"b", echo,
			"took", time.Since(begin),
		)
	}(time.Now())

	echo = mw.next.Echo(s)
	return
}

func (mw loggingMetaMiddleware) Rtt(t int64) (clock int64) {
	defer func(begin time.Time) {
		_ = mw.logger.Log(
			"method", "rtt",
			"input", t,
			"b", clock,
			"took", time.Since(begin),
		)
	}(time.Now())

	clock = mw.next.Rtt(t)
	return
}
